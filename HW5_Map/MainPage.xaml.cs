﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProductModel;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace HW5_Map
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public List<ProductData> product;

        public string fileName = "./products.json";
        public MainPage()
        {
            InitializeComponent();
            LoadJson();
        }

        public void LoadJson()
        {
            using (StreamReader r = new StreamReader(fileName))
            {
                string json = r.ReadToEnd();
                product = JsonConvert.DeserializeObject<List<ProductData>>(json);
            }
            ProductsListView.ItemsSource = product;
        }

        private async void Handle_Clicked(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var itemClicked = menuItem.CommandParameter as ProductData;
            await Navigation.PushAsync(new ItemPage(itemClicked));
        }
    }
}
