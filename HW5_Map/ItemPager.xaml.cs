﻿using System;
using System.Collections.Generic;
using ProductModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW5_Map
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemPager : ContentPage
    {
        public ItemPager(ProductData selectedItem)
        {
            InitializeComponent();

            itemImage.Source = selectedItem.ImageUrl;
            itemName.Text = "Name: " + selectedItem.ProductName;
            itemDescription.Text = "Description: " + selectedItem.Description;
            itemStars.Text = "Rating: " + selectedItem.StarRating;
            itemPrice.Text = "$" + selectedItem.Price;

        }
    }
}