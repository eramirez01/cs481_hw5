﻿
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ProductModel
{

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class ProductData
    {
        [JsonProperty("productId")]
        public long ProductID
        {
            get;
            set;
        }

        [JsonProperty("productName")]
        public string ProductName
        {
            get;
            set;
        }

        [JsonProperty("productCode")]
        public string ProductCode
        {
            get;
            set;
        }

        [JsonProperty("imageUrl")]
        public Uri ImageUrl
        {
            get;
            set;
        }

        [JsonProperty("starRating")]
        public double StarRating
        {
            get;
            set;
        }

        [JsonProperty("price")]
        public double Price
        {
            get;
            set;
        }

        [JsonProperty("description")]
        public string Description
        {
            get;
            set;
        }

        [JsonProperty("releaseDate")]
        public string ReleaseDate
        {
            get;
            set;
        }

    }

    public partial class ProductData
    {
        public static ProductData[] FromJson(string json) => json.Convert.DeserializeObject<ProductData[]>(json, ProductModel.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore;
            DateParseHandling = DateParseHandingling.None;
            Converters =
            {
                new IsoDateTimeConverter
                {
                    DateTimeStyles = DateTimeStyles.AssumeUniversal

                }
            },

        };

    }
}